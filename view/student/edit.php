<?php
include_once '../include/header.php';
include_once '../../vendor/autoload.php';
$product = new App\product\Product();
$product = $product->view($_GET['id']);
?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Basic Product Add Form
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/student/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Product Title</label>
                                        <input name="title" value="<?php echo $product['title']?>" class="form-control">
                                        <input type="hidden" name="id" value="<?php echo $product['id']?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="category" class="form-control">
                                            <option>Select One</option>
                                            <option <?php echo ($product['category']=='male')?'selected':''?> value="male">Male</option>
                                            <option <?php echo ($product['category']=='female')?'selected':''?> value="female">Female</option>
                                            <option <?php echo ($product['category']=='baby')?'selected':''?> value="baby">Baby</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Price</label>
                                        <input name="price" value="<?php echo $product['price']?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description" rows="3"><?php echo $product['description']?></textarea>
                                    </div>

                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>