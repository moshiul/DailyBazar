<?php
include_once '../include/header.php';
include_once '../../vendor/autoload.php';
$products = new App\product\Product();
$product = $products->index();

?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <?php
        foreach ($product as $product){



        ?>

        <div class="row">
            <div class="col-md-3 col-sm-6">
    		<span class="thumbnail">
      			<img src="assets/images/<?php echo $product['image']?>" height="50" width="50" alt="...">
      			<h4><?php echo $product['title']?></h4>
      			<div class="ratings">
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star-empty"></span>
                </div>
      			<p><?php echo $product['description']?></p>
      			<hr class="line">
      			<div class="row">
      				<div class="col-md-6 col-sm-6">
      					<p class="price">$<?php echo $product['price']?></p>
      				</div>
      				<div class="col-md-6 col-sm-6">
      				 <a href="view/student/view.php?id=<?php echo $product['id']?>">view</a>
      				</div>

      			</div>
    		</span>
            </div>

            <?php
        }
            ?>
            <!-- END PRODUCTS -->
        </div>


        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>