<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/25/2017
 * Time: 2:11 PM
 */

namespace App\product;
use App\Connection;
use PDO;
use PDOException;

class Product extends Connection
{
    private $title;
    private $category;
    private $price;
    private $description;
    private $image;
    private $id;


    public function set($data=array())
    {
        if(array_key_exists('title',$data)){
            $this->title = $data['title'];
        }
        if(array_key_exists('category',$data)){
            $this->category = $data['category'];
        }
        if(array_key_exists('price',$data)){
            $this->price = $data['price'];
        }
        if(array_key_exists('description',$data)){
            $this->description = $data['description'];
        }
        if(array_key_exists('image',$data)){
            $this->image = $data['image'];
        }
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }

        //return $this;
    }


    public function store(){
        try {
            $stmt = $this->con->prepare("INSERT INTO `product` (`id`, `title`, `category`, `price`, `description`, `image`) VALUES (NULL, :title, :category, :price, :description, :image);");


            $result = $stmt->execute(array(
                ':title'=>$this->title,
                ':category'=>$this->category,
                ':price'=>$this->price,
                ':description'=>$this->description,
                ':image'=>$this->image,
            ));

            if($result){
                header('location: index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function index(){
        try {

            $stmt = $this->con->prepare("SELECT * FROM `product`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        try {

            $stmt = $this->con->prepare("SELECT * FROM `product`  WHERE id = :id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function delete($id){
        try {

            $stmt = $this->con->prepare("DELETE FROM `product` WHERE id = :id"); //update table name
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                header('location: index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function update(){
        try {

            $stmt = $this->con->prepare("UPDATE `product` SET `title` = :title, `category` = :category, `price` = :price, `description` = :description WHERE `product`.`id` = :id;"); //update table name
            $stmt->bindValue(':title', $this->title, PDO::PARAM_INT);
            $stmt->bindValue(':category', $this->category, PDO::PARAM_INT);
            $stmt->bindValue(':price', $this->price, PDO::PARAM_INT);
            $stmt->bindValue(':description', $this->description, PDO::PARAM_INT);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                header('location: index.php');
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}
