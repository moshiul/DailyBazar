<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 9/25/2017
 * Time: 2:04 PM
 */

namespace App;
use PDO;
use PDOException;

class Connection
{
    protected $con;
    private $user="root";
    private $pass="";

    /**
     * @return mixed
     */
    public function __construct()
    {
        try {
            $this->con = new PDO('mysql:host=localhost;dbname=product', $this->user, $this->pass);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}